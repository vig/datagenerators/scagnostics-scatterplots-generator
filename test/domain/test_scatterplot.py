
import uuid
import pytest
from scagnosticscnn.domain.scatterplot import ScatterPlot


def test_scatterplot_model_init():
    data = {
        'id': uuid.uuid4(),
        'name': "numpy.random.random_integers",
        'domainMin': 0,
        'domainMax': 100,
        'numPoints': 2,
        'numClasses': 2,
        'timestamp': 100015,
        'data': {
            'points': [[99, 99], [20, 20]],
            'classes': [1, 2],
        }
    }

    # self, id, domainMin, domainMax, points, classes, **kwargs
    scatterplot = ScatterPlot(
        id=data['id'],
        domainMin=data['domainMin'],
        domainMax=data['domainMax'],
        points=data['data']['points'],
        classes=data['data']['classes'])
    assert scatterplot.id == data['id']
    assert scatterplot.domainMin == data['domainMin']
    assert scatterplot.domainMax == data['domainMax']
    assert scatterplot.points == data['data']['points']
    assert scatterplot.classes == data['data']['classes']

    scatterplot = ScatterPlot(
        id=data['id'],
        domainMin=data['domainMin'],
        domainMax=data['domainMax'],
        points=data['data']['points'],
        classes=data['data']['classes'],
        timestamp=data['timestamp'])
    assert scatterplot.timestamp == data['timestamp']
    assert scatterplot.numClasses == len(data['data']['classes'])
    assert scatterplot.numPoints == len(data['data']['points'])


def test_scatterplot_model_from_dict():
    scatterplot1_dict = {
        'id': '3251a5bd-86be-428d-8ae9-6e51a8048c33',
        'domainMin': 0,
        'domainMax': 512,
        'points': [[10, 12], [256, 512]],
        'numPoints': 2,
        'classes': [1, 2],
        'numClasses': 2,
        'timestamp': 54451215,
        'datasetName': 'Fake TEST'
    }

    scatterplot = ScatterPlot.from_dict(scatterplot1_dict)

    assert scatterplot.id == scatterplot1_dict['id']
    assert scatterplot.domainMin == 0
    assert scatterplot.domainMax == 512
    assert scatterplot.points == [[10, 12], [256, 512]]
    assert scatterplot.classes == [1, 2]
    assert scatterplot.datasetName == 'Fake TEST'


def test_scatterplot_model_to_dict():
    scatterplot1_dict = {
        'id': '3251a5bd-86be-428d-8ae9-6e51a8048c33',
        'domainMin': 0,
        'domainMax': 1000,
        'points': [[10, 12], [256, 512]],
        'numPoints': 2,
        'classes': [1, 2],
        'numClasses': 2,
        'timestamp': 54451215,
        'datasetName': 'Fake TEST'
    }

    scatterplot = ScatterPlot.from_dict(scatterplot1_dict)

    assert scatterplot.to_dict() == scatterplot1_dict


def test_scatterplot_model_comparison():
    scatterplot1_dict = {
        'id': '3251a5bd-86be-428d-8ae9-6e51a8048c33',
        'domainMin': 0,
        'domainMax': 512,
        'points': [[10, 12], [256, 512]],
        'numPoints': 2,
        'classes': [1, 2],
        'numClasses': 2,
        'timestamp': 54451215,
        'datasetName': 'Fake TEST'
    }
    scatterplot1 = ScatterPlot.from_dict(scatterplot1_dict)
    scatterplot2 = ScatterPlot.from_dict(scatterplot1_dict)
    assert scatterplot1 == scatterplot2

    scatterplot2_dict = {
        'id': '3251a5bd-86be-428d-8adsfae9',
        'domainMin': 0,
        'domainMax': 55,
        'points': [[55, 1], [2, 5]],
        'numPoints': 2,
        'classes': [1, 2],
        'numClasses': 2,
        'timestamp': 54451215,
        'datasetName': 'Fake TEST'
    }

    scatterplot2 = ScatterPlot.from_dict(scatterplot2_dict)
    assert scatterplot1 != scatterplot2


def test_scatterplot_minmax_domain():
    scatterplot1_dict = {
        'id': '3251a5bd-86be-428d-8ae9-6e51a8048c33',
        'domainMin': 0,
        'domainMax': 0,
        'points': [[10, 12], [256, 512]],
        'numPoints': 2,
        'classes': [1, 2],
        'numClasses': 2,
        'timestamp': 54451215,
        'datasetName': 'Fake TEST'
    }

    with pytest.raises(ValueError):
        ScatterPlot.from_dict(scatterplot1_dict)

    scatterplot2_dict = {
        'id': '3251a5bd-86be-428d-8ae9-6e51a8048c33',
        'domainMin': 10,
        'domainMax': 0,
        'points': [[10, 12], [256, 512]],
        'numPoints': 2,
        'classes': [1, 2],
        'numClasses': 2,
        'timestamp': 54451215,
        'datasetName': 'Fake TEST'
    }

    with pytest.raises(ValueError):
        ScatterPlot.from_dict(scatterplot2_dict)


def test_scatterplot_out_of_domain():
    scatterplot3_dict = {
        'id': '3251a5bd-86be-428d-8ae9-6e51a8048c33',
        'domainMin': 100,
        'domainMax': 200,
        'points': [[10, 12], [256, 512]],
        'numPoints': 2,
        'classes': [1, 2],
        'numClasses': 2,
        'timestamp': 54451215,
        'datasetName': 'Fake TEST'
    }

    with pytest.raises(ValueError):
        ScatterPlot.from_dict(scatterplot3_dict)
