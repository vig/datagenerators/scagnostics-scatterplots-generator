from scagnosticscnn.domain.qualitymetrics import scagnosticservice as ss
from scagnosticscnn.domain.generator import datagenerator as dg
from scagnosticscnn.domain.scatterplot import ScatterPlot
from pyscagnostics import scagnostics
import numpy as np
import pandas as pd


def test_scag_from_dataframe():
    assert(ss.ScagnosticsService())
    scag = ss.ScagnosticsService()

    x = np.random.uniform(0, 1, 100)
    y = np.random.uniform(0, 1, 100)
    z = np.random.uniform(0, 1, 100)
    df = pd.DataFrame({
        'x': x,
        'y': y,
        'z': z
    })
    result = scag._calculateQMDataFrame(df)
    assert result


def test_init_with_params():
    assert(ss.ScagnosticsService(scagnostics_bins=1000, scagnostics_remove_outliers=False))
    scagService = ss.ScagnosticsService(scagnostics_bins=1000, scagnostics_remove_outliers=False)
    assert scagService.bins == 1000
    assert scagService.remove_outliers == False


def test_scatterplot_to_dataframe():
    generator = dg.RandomDataGenerator(101)
    scatterPlotData = generator.scatterplot()
    scatterPlot = ScatterPlot.from_dict(scatterPlotData[0])
    dataFrame = ss.ScagnosticsService.scatterplot_to_dataframe(scatterPlot)
    assert not dataFrame.empty

    assert len(dataFrame.columns) == 2
    assert len(dataFrame.index) == len(scatterPlot.points)


def test_scag_from_scatterplot():
    generator = dg.RandomDataGenerator(101)
    scatterPlotData = generator.scatterplot()
    scatterPlot = ScatterPlot.from_dict(scatterPlotData[0])

    scagService = ss.ScagnosticsService()
    results = scagService.calculateQM(scatterPlot)

    assert results


def test_scag_getSimpleResults():
    generator = dg.RandomDataGenerator(101)
    scatterPlotData = generator.scatterplot()
    scatterPlot = ScatterPlot.from_dict(scatterPlotData[0])

    scagService = ss.ScagnosticsService()
    simpleResults = scagService.calculateQM(scatterPlot)

    assert simpleResults
    dict = simpleResults.to_dict()
    assert dict
    assert dict['Clumpy'] != None
    assert len(dict) == 11


def test_scag_severaltimes():
    generator = dg.RandomDataGenerator(101)
    scagService = ss.ScagnosticsService()

    results = []
    for i in range(10):
        scatterPlotData = generator.scatterplot()
        scatterPlot = ScatterPlot.from_dict(scatterPlotData[0])

        simpleResults = scagService.calculateQM(scatterPlot)

        assert simpleResults.to_dict()
        results.append(simpleResults.to_dict())

        assert simpleResults
        assert simpleResults['Clumpy'] != None
        assert len(simpleResults) == 11
    assert len(results) == 10
