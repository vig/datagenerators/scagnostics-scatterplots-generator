from scagnosticscnn.domain.qualitymetrics.scagnosticservice import ScagnosticsService, ScagnosticsServiceBuilder
from scagnosticscnn.domain.qualitymetrics.qualitymetrics import QualityMetricsFactory


def test_register_service():
    factory = QualityMetricsFactory()
    factory.register_builder(
        'SCAGNOSTICS', ScagnosticsServiceBuilder())

    config = {
        'scagnostics_bins': 100,
        'scagnostics_remove_outliers': False,
    }

    scagnosticsService = factory.create('SCAGNOSTICS', **config)
    assert scagnosticsService
    assert scagnosticsService.bins == 100
    assert scagnosticsService.remove_outliers == False

    config = {
        'scagnostics_bins': 10,
        'scagnostics_remove_outliers': True,
    }

    scagnosticsService = factory.create('SCAGNOSTICS', **config)
    assert scagnosticsService
    assert scagnosticsService.bins == 10
    assert scagnosticsService.remove_outliers == True


def test_get_service_without_config():
    factory = QualityMetricsFactory()
    factory.register_builder(
        'SCAGNOSTICS', ScagnosticsServiceBuilder())

    scagnosticsService = factory.create('SCAGNOSTICS')
    assert scagnosticsService
    assert scagnosticsService.bins == 50
    assert scagnosticsService.remove_outliers == True
