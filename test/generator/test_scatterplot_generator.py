
# import uuid
from scagnosticscnn.domain.scatterplot import ScatterPlot
from scagnosticscnn.domain.generator.datagenerator import RandomDataGenerator

import json


def test_generator_init():
    generator = RandomDataGenerator()
    assert generator

    generator = RandomDataGenerator(100)
    assert generator


def test_generator_scatterplot_without_filters():
    generator = RandomDataGenerator()

    scatterplotData = generator.scatterplot()
    assert scatterplotData
    assert len(scatterplotData) == 1
    assert scatterplotData[0]

    print(json.dumps(scatterplotData[0], sort_keys=True, indent=4, separators=(',', ': ')))
    scatterPlot = ScatterPlot.from_dict(scatterplotData[0])
    assert scatterPlot
    assert scatterPlot.numClasses == 1
    assert scatterPlot.domainMin == 0
    assert scatterPlot.domainMax == 1000
    assert scatterPlot.numPoints == 100


def test_generator_scatterplot_with_filters():
    generator = RandomDataGenerator()

    filters = {
        "count": 10,
        "numClasses": 10,
        "domainMin": 50,
        "domainMax": 1200,
        "numPoints": 60,
    }

    scatterplotData = generator.scatterplot(filters)
    assert scatterplotData
    assert len(scatterplotData) == filters["count"]

    scatterPlot = ScatterPlot.from_dict(scatterplotData[0])
    assert scatterPlot
    assert scatterPlot.numClasses == filters["numClasses"]
    assert scatterPlot.domainMin == filters["domainMin"]
    assert scatterPlot.domainMax == filters["domainMax"]
    assert scatterPlot.numPoints == filters["numPoints"]
