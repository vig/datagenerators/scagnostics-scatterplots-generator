import pytest

from scagnosticscnn.domain.storageroom import StorageRoom
from scagnosticscnn.shared.domain_model import DomainModel

from scagnosticscnn.repository import memrepo_scatterplot as memrepo


@pytest.fixture
def scatterplot_dicts():
    return [
        {"id": "a909c580-5349-27eb-a80a-45fd9b719ed2", "datasetName": "numpy.random.random_integers", "domainMin": 0, "domainMax": 1000, "numPoints": 100, "numClasses": 1, "timestamp": "2002-05-06T20:19:52Z", "points": [[741, 993], [593, 836], [982, 592], [469, 255], [999, 673], [552, 389], [512, 171], [779, 281], [450, 398], [456, 896], [164, 962], [768, 339], [162, 205], [800, 576], [495, 916], [541, 418], [519, 212], [819, 581], [362, 246], [530, 702], [695, 207], [757, 941], [598, 102], [878, 895], [160, 906], [136, 725], [567, 281], [801, 93], [697, 410], [736, 857], [459, 417], [872, 335], [879, 346], [684, 203], [993, 793], [8, 415], [854, 516], [182, 883], [2, 475], [362, 695], [339, 86], [762, 586], [419, 319], [627, 961], [629, 727], [632, 94], [346, 373], [481, 17], [128, 477], [913, 729], [499, 753], [879, 916], [653, 3], [232, 957], [
            421, 805], [829, 810], [913, 769], [546, 127], [66, 590], [196, 609], [661, 802], [615, 854], [584, 379], [369, 58], [902, 70], [699, 380], [183, 287], [71, 531], [755, 424], [475, 0], [779, 931], [389, 384], [777, 601], [921, 672], [191, 997], [789, 315], [742, 270], [587, 735], [728, 504], [237, 653], [587, 575], [322, 937], [38, 583], [408, 715], [54, 445], [703, 840], [309, 68], [418, 272], [518, 12], [240, 604], [110, 185], [28, 704], [255, 98], [819, 355], [384, 49], [722, 598], [192, 40], [738, 628], [252, 260], [155, 766]], "classes": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {"id": "b0ddfc2d-234d-1a47-f96c-28ffd976e464", "datasetName": "numpy.random.random_integers", "domainMin": 0, "domainMax": 1000, "numPoints": 100, "numClasses": 1, "timestamp": "2025-02-10T04:27:49Z", "points": [[741, 993], [593, 836], [982, 592], [469, 255], [999, 673], [552, 389], [512, 171], [779, 281], [450, 398], [456, 896], [164, 962], [768, 339], [162, 205], [800, 576], [495, 916], [541, 418], [519, 212], [819, 581], [362, 246], [530, 702], [695, 207], [757, 941], [598, 102], [878, 895], [160, 906], [136, 725], [567, 281], [801, 93], [697, 410], [736, 857], [459, 417], [872, 335], [879, 346], [684, 203], [993, 793], [8, 415], [854, 516], [182, 883], [2, 475], [362, 695], [339, 86], [762, 586], [419, 319], [627, 961], [629, 727], [632, 94], [346, 373], [481, 17], [128, 477], [913, 729], [499, 753], [879, 916], [653, 3], [232, 957], [
            421, 805], [829, 810], [913, 769], [546, 127], [66, 590], [196, 609], [661, 802], [615, 854], [584, 379], [369, 58], [902, 70], [699, 380], [183, 287], [71, 531], [755, 424], [475, 0], [779, 931], [389, 384], [777, 601], [921, 672], [191, 997], [789, 315], [742, 270], [587, 735], [728, 504], [237, 653], [587, 575], [322, 937], [38, 583], [408, 715], [54, 445], [703, 840], [309, 68], [418, 272], [518, 12], [240, 604], [110, 185], [28, 704], [255, 98], [819, 355], [384, 49], [722, 598], [192, 40], [738, 628], [252, 260], [155, 766]], "classes": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {"id": "7c0b78fe-2d39-c413-2fad-b1f9dbf9c815", "datasetName": "numpy.random.random_integers", "domainMin": 0, "domainMax": 1000, "numPoints": 100, "numClasses": 1, "timestamp": "2021-01-24T02:33:33Z", "points": [[741, 993], [593, 836], [982, 592], [469, 255], [999, 673], [552, 389], [512, 171], [779, 281], [450, 398], [456, 896], [164, 962], [768, 339], [162, 205], [800, 576], [495, 916], [541, 418], [519, 212], [819, 581], [362, 246], [530, 702], [695, 207], [757, 941], [598, 102], [878, 895], [160, 906], [136, 725], [567, 281], [801, 93], [697, 410], [736, 857], [459, 417], [872, 335], [879, 346], [684, 203], [993, 793], [8, 415], [854, 516], [182, 883], [2, 475], [362, 695], [339, 86], [762, 586], [419, 319], [627, 961], [629, 727], [632, 94], [346, 373], [481, 17], [128, 477], [913, 729], [499, 753], [879, 916], [653, 3], [232, 957], [
            421, 805], [829, 810], [913, 769], [546, 127], [66, 590], [196, 609], [661, 802], [615, 854], [584, 379], [369, 58], [902, 70], [699, 380], [183, 287], [71, 531], [755, 424], [475, 0], [779, 931], [389, 384], [777, 601], [921, 672], [191, 997], [789, 315], [742, 270], [587, 735], [728, 504], [237, 653], [587, 575], [322, 937], [38, 583], [408, 715], [54, 445], [703, 840], [309, 68], [418, 272], [518, 12], [240, 604], [110, 185], [28, 704], [255, 98], [819, 355], [384, 49], [722, 598], [192, 40], [738, 628], [252, 260], [155, 766]], "classes": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {"id": "0f9bdcd7-83a2-99e7-00a0-03fb17b75469", "datasetName": "numpy.random.random_integers", "domainMin": 0, "domainMax": 1000, "numPoints": 100, "numClasses": 1, "timestamp": "2027-09-05T09:30:28Z", "points": [[741, 993], [593, 836], [982, 592], [469, 255], [999, 673], [552, 389], [512, 171], [779, 281], [450, 398], [456, 896], [164, 962], [768, 339], [162, 205], [800, 576], [495, 916], [541, 418], [519, 212], [819, 581], [362, 246], [530, 702], [695, 207], [757, 941], [598, 102], [878, 895], [160, 906], [136, 725], [567, 281], [801, 93], [697, 410], [736, 857], [459, 417], [872, 335], [879, 346], [684, 203], [993, 793], [8, 415], [854, 516], [182, 883], [2, 475], [362, 695], [339, 86], [762, 586], [419, 319], [627, 961], [629, 727], [632, 94], [346, 373], [481, 17], [128, 477], [913, 729], [499, 753], [879, 916], [653, 3], [232, 957], [
            421, 805], [829, 810], [913, 769], [546, 127], [66, 590], [196, 609], [661, 802], [615, 854], [584, 379], [369, 58], [902, 70], [699, 380], [183, 287], [71, 531], [755, 424], [475, 0], [779, 931], [389, 384], [777, 601], [921, 672], [191, 997], [789, 315], [742, 270], [587, 735], [728, 504], [237, 653], [587, 575], [322, 937], [38, 583], [408, 715], [54, 445], [703, 840], [309, 68], [418, 272], [518, 12], [240, 604], [110, 185], [28, 704], [255, 98], [819, 355], [384, 49], [722, 598], [192, 40], [738, 628], [252, 260], [155, 766]], "classes": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]},
        {"id": "71ea94f7-158a-6d57-6702-321e56f8796b", "datasetName": "numpy.random.random_integers", "domainMin": 50, "domainMax": 1200, "numPoints": 60, "numClasses": 10, "timestamp": "2022-09-04T16:39:31Z", "points": [[791, 1043], [886, 519], [439, 562], [506, 1068], [214, 212], [626, 966], [869, 631], [412, 296], [257, 807], [991, 152], [945, 210], [956, 617], [143, 747], [460, 509], [467, 385], [929, 734], [253, 1082], [566, 232], [933, 52], [389, 1160], [812, 469], [369, 1011], [682, 1168], [396, 531], [1091, 963], [779, 53], [282, 471], [855, 879], [860, 819], [
            596, 177], [116, 640], [246, 659], [711, 852], [665, 904], [429, 419], [1132, 952], [1144, 749], [1145, 581], [805, 474], [525, 1074], [829, 827], [365, 792], [320, 637], [785, 778], [287, 703], [372, 1112], [633, 1070], [104, 495], [1142, 468], [322, 62], [1063, 290], [654, 1184], [78, 305], [148, 869], [405, 1123], [772, 242], [1114, 678], [1069, 310], [205, 816], [356, 105]], "classes": [1, 2, 5, 6, 2, 5, 9, 5, 2, 7, 8, 0, 3, 4, 6, 6, 4, 5, 1, 1, 9, 4, 9, 1, 9, 0, 9, 7, 1, 8, 7, 6, 8, 3, 0, 0, 5, 6, 6, 8, 2, 2, 1, 0, 0, 2, 3, 1, 6, 3, 2, 2, 0, 3, 3, 3, 4, 7, 5, 6]}
    ]



def _check_results(domain_models_list, data_list):
    assert len(domain_models_list) == len(data_list)
    assert all([isinstance(dm, DomainModel) for dm in domain_models_list])
    assert set([dm.id for dm in domain_models_list]
               ) == set([d['id'] for d in data_list])


def test_repository_list_without_parameters(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)

    _check_results(
        repo.list(),
        scatterplot_dicts
    )


def test_repository_list_with_filters_unknown_key(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)

    with pytest.raises(KeyError):
        repo.list(filters={'name': 'aname'})


def test_repository_list_with_filters_unknown_operator(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)

    with pytest.raises(ValueError):
        repo.list(filters={'price__in': [20, 30]})


def test_repository_list_with_filters_price(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)

    _check_results(
        repo.list(filters={'numPoints': 60}),
        [scatterplot_dicts[4]]
    )


def test_repository_list_with_filters_numPoints_eq(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)

    _check_results(
        repo.list(filters={'numPoints__eq': 60}),
        [scatterplot_dicts[4]]
    )


def test_repository_list_with_filters_numPoints_lt(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)

    _check_results(
        repo.list(filters={'numPoints__lt': 200}),
        [scatterplot_dicts[0], scatterplot_dicts[1], scatterplot_dicts[2],
            scatterplot_dicts[3], scatterplot_dicts[4]]
    )


def test_repository_list_with_filters_numPoints_gt(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)
    _check_results(
        repo.list(filters={'numPoints__gt': 60}),
        [scatterplot_dicts[0], scatterplot_dicts[1], scatterplot_dicts[2], scatterplot_dicts[3]]
    )


def test_repository_list_with_filters_domainMin(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)

    _check_results(
        repo.list(filters={'domainMin': 50}),
        [scatterplot_dicts[4]]
    )


def test_repository_list_with_filters_domainMin_eq(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)
    _check_results(
        repo.list(filters={'domainMin__eq': 50}),
        [scatterplot_dicts[4]]
    )


def test_repository_list_with_filters_domainMin_lt(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)
    _check_results(
        repo.list(filters={'domainMin__lt': 60}),
        [scatterplot_dicts[0], scatterplot_dicts[1], scatterplot_dicts[2],
            scatterplot_dicts[3], scatterplot_dicts[4]]
    )


def test_repository_list_with_filters_domainMin_gt(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)
    _check_results(
        repo.list(filters={'domainMin__gt': 400}),
        []
    )


def test_repository_list_with_filters_id(scatterplot_dicts):
    repo = memrepo.MemRepo(scatterplot_dicts)

    _check_results(
        repo.list(filters={'id': 'b0ddfc2d-234d-1a47-f96c-28ffd976e464'}),
        [scatterplot_dicts[1]]
    )
