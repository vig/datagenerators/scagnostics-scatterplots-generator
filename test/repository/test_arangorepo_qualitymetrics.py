import pytest

from scagnosticscnn.domain.scatterplot import ScatterPlot
from scagnosticscnn.domain.qmdata import QualityMetricsData
from scagnosticscnn.shared.domain_model import DomainModel

from scagnosticscnn.repository import arangorepo_scatterplot as arangorepo

from scagnosticscnn.settings import TestConfig as config

from arango_crud import (
    Config, BasicAuth, RandomCluster, StepBackOffStrategy
)

import json


@pytest.fixture
def qm_dicts():
    return [
        {"index": 0, "id": "a909c580-5349-27eb-a80a-45fd9b719ed2", "type": "scagnostics", "Outlying": 1.222222222, "Skewed": 0.45, "Clumpy": 0.0, "Sparse": 0.12972969309001994,
            "Striated": 0.04918032786885246, "Convex": 0.45949744337317133, "Skinny": 0.6596406289456485, "Stringy": 0.37856736946534014, "Monotonic": 0.004230256843235436},
        {"index": 1, "id": "36285d5e-dde7-7816-540a-c68ebf2c7449", "type": "Scagnostics", "Outlying": 2.163851567e-315, "Skewed": 0.49660553873685853, "Clumpy": 0.0, "Sparse": 0.10917914543912022,
            "Striated": 0.06060606060606061, "Convex": 0.49149968327774035, "Skinny": 0.6219836676064829, "Stringy": 0.4603712890866645, "Monotonic": 0.004514230759568579},
        {"index": 2, "id": "81fe1d05-e673-7a2a-fe80-f5e6af6b00c6", "type": "Scagnostics", "Outlying": 2.092172286e-315, "Skewed": 0.6477868703548296, "Clumpy": 0.0, "Sparse": 0.12116216241298977,
            "Striated": 0.0707070707070707, "Convex": 0.5622619469293321, "Skinny": 0.6274260518513906, "Stringy": 0.41114988452266565, "Monotonic": 0.00011829589517417158},
        {"index": 3, "id": "ef6cf719-f218-e502-3f26-263facfcfb93", "type": "Scagnostics", "Outlying": 2.07447371e-315, "Skewed": 0.5452102206519984, "Clumpy": 0.0, "Sparse": 0.11071210499447515,
            "Striated": 0.1414141414141414, "Convex": 0.602169338689844, "Skinny": 0.6239805113098864, "Stringy": 0.33329303703703705, "Monotonic": 0.00011416339098260884},
        {"index": 4, "id": "958f5d4c-65a0-57ee-761e-1d50047f7925", "type": "Scagnostics", "Outlying": 2.16327399e-315, "Skewed": 0.49660553873685853, "Clumpy": 0.0, "Sparse": 0.10917914543912022,
            "Striated": 0.06060606060606061, "Convex": 0.49149968327774035, "Skinny": 0.6219836676064829, "Stringy": 0.4603712890866645, "Monotonic": 0.004514230759568579},
        {"index": 5, "id": "2a025a9d-bea5-39c3-0fa5-055bd7b4b3fc", "type": "Scagnostics", "Outlying": 2.09161399e-315, "Skewed": 0.6477868703548296, "Clumpy": 0.0, "Sparse": 0.12116216241298977,
            "Striated": 0.0707070707070707, "Convex": 0.5622619469293321, "Skinny": 0.6274260518513906, "Stringy": 0.41114988452266565, "Monotonic": 0.00011829589517417158},
        {"index": 6, "id": "27f76aa3-29d9-b39b-f9ff-d208e01c71b5", "type": "Scagnostics", "Outlying": 2.073924846e-315, "Skewed": 0.5452102206519984, "Clumpy": 0.0, "Sparse": 0.11071210499447515,
         "Striated": 0.1414141414141414, "Convex": 0.602169338689844, "Skinny": 0.6239805113098864, "Stringy": 0.33329303703703705, "Monotonic": 0.00011416339098260884},
        {"index": 7, "id": "f7c6707a-2881-f7d9-4544-97aa2001b923", "type": "Scagnostics", "Outlying": 2.27924519e-315, "Skewed": 0.6052901651607443, "Clumpy": 0.0, "Sparse": 0.11114809380909979,
            "Striated": 0.10526315789473684, "Convex": 0.49729897645891896, "Skinny": 0.5551197073726101, "Stringy": 0.4570138984857758, "Monotonic": 0.009201322977593438},
        {"index": 8, "id": "8475290d-89e4-4f85-8c9a-13990ffc9d38", "type": "Scagnostics", "Outlying": 2.213434454e-315, "Skewed": 0.6195660791445634, "Clumpy": 0.0, "Sparse": 0.1185700714730004,
            "Striated": 0.1326530612244898, "Convex": 0.50517412887852, "Skinny": 0.689221359013203, "Stringy": 0.5454850480985475, "Monotonic": 0.018317164105752456},
        {"index": 9, "id": "b29ea488-edd7-1969-2144-ad4ac67e3e97", "type": "Scagnostics", "Outlying": 2.07172903e-315, "Skewed": 0.5209506993391539, "Clumpy": 0.0, "Sparse": 0.11647499953960712,
            "Striated": 0.041237113402061855, "Convex": 0.664348310616724, "Skinny": 0.6066296575198864, "Stringy": 0.3409902395011015, "Monotonic": 0.005252499625191035},
        {"index": 10, "id": "d0d6bf78-2a36-a9c2-d35b-eda559a8cd37", "type": "Scagnostics", "Outlying": 2.18060759e-315, "Skewed": 0.5965419189499028, "Clumpy": 0.0, "Sparse": 0.11809530212762649,
            "Striated": 0.031914893617021274, "Convex": 0.4137792678510403, "Skinny": 0.7265563064112626, "Stringy": 0.277389660557245, "Monotonic": 0.0002794190741561684},
        {"index": 11, "id": "9f6c3c26-3d1b-cd0d-223f-ad28b8615b70", "type": "Scagnostics", "Outlying": 0.08547782968893519, "Skewed": 0.4892737504900986, "Clumpy": 0.0, "Sparse": 0.11044260845938768,
         "Striated": 0.031578947368421054, "Convex": 0.6728361450598748, "Skinny": 0.522068912765995, "Stringy": 0.4971842123132392, "Monotonic": 0.03586339383552079},
        {"index": 12, "id": "aa56784a-6e89-9c03-9c68-8e87d160772d", "type": "Scagnostics", "Outlying": 0.025691946625736924, "Skewed": 0.49178808465107116, "Clumpy": 0.0, "Sparse": 0.12138372087025381,
            "Striated": 0.10204081632653061, "Convex": 0.5434118854114491, "Skinny": 0.6649790239953726, "Stringy": 0.34698833237912863, "Monotonic": 0.0040636139277080985},
        {"index": 13, "id": "0280c55e-1af6-92e8-ba5c-bccd24da6270", "type": "Scagnostics", "Outlying": 0.029914527982123122, "Skewed": 0.5218943263012789, "Clumpy": 0.0, "Sparse": 0.09985418696202053,
            "Striated": 0.07216494845360824, "Convex": 0.5497510535539428, "Skinny": 0.5862682657350652, "Stringy": 0.3409902395011015, "Monotonic": 0.005412337063012171}
    ]


@pytest.fixture
def setupConnection():
    configArango = Config(
        cluster=RandomCluster(urls=['http://' + config.ARANBGO_DB_URL + ':' +
                                    str(config.ARANBGO_DB_PORT)]),  # see Cluster Styles
        timeout_seconds=3,
        back_off=StepBackOffStrategy([0.1, 0.5, 1, 1, 1]),  # see Back Off Strategies
        # auth=BasicAuth(username='root', password='rootPassword'),
        auth=BasicAuth(username=config.ARANBGO_DB_USER, password=config.ARANBGO_DB_PW),
        ttl_seconds=31622400
    )
    return configArango


def _check_results(domain_models_list, data_list):
    assert len(domain_models_list) == len(data_list)
    assert all([isinstance(dm, DomainModel) for dm in domain_models_list])
    dmSet = set([dm.id for dm in domain_models_list])
    idSet = set([d['id'] for d in data_list])
    assert dmSet == idSet


# ssh -L 8529:localhost:8529 dev@131.211.81.100
def test_connection_config(setupConnection):
    assert config.ENV == 'test'
    assert config.TESTING == True
    assert config.DEBUG == True
    # assert config.ARANBGO_DB_URL == 'arangodb'
    assert config.ARANBGO_DB_PORT == 8529

    # assert config.ARANBGO_DB_URL + ':' + str(config.ARANBGO_DB_PORT) == 'arangodb:8529'

    assert setupConnection
    setupConnection.prepare()  # recommended, not required


def test_connection_exists(setupConnection):
    # validate that it exists and can be connected
    setupConnection.prepare()
    db = setupConnection.database('scatterplots')
    assert db


def test_connection_available(setupConnection):
    # validate that it exists and can be connected
    setupConnection.prepare()
    db = setupConnection.database('scatterplots')
    assert db

    # validate that repo can connect
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')


def test_repository_create_collection_and_clear(setupConnection):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    _check_results(
        repo.listQualityMetrics(),
        []
    )

###################


def test_repository_create(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    repo.createQualityMetric(QualityMetricsData.from_dict(qm_dicts[0]))

    dataList = [qm_dicts[0]]

    _check_results(
        repo.listQualityMetrics(),
        dataList
    )

###################


def test_repository_list_without_parameters(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    for data in qm_dicts:
        repo.createQualityMetric(QualityMetricsData.from_dict(data))

    result = repo.listQualityMetrics()
    assert (result)

    _check_results(
        repo.listQualityMetrics(),
        qm_dicts
    )


def test_repository_list_with_filters_unknown_key(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    for data in qm_dicts:
        repo.createQualityMetric(QualityMetricsData.from_dict(data))

    _check_results(
        repo.listQualityMetrics(filters={'name': 'aname'}),
        []
    )


def test_repository_list_with_filters_known_key(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    for data in qm_dicts:
        repo.createQualityMetric(QualityMetricsData.from_dict(data))

    result = repo.listQualityMetrics(filters={'Outlying': 2.092172286e-315})
    assert (result)

    expected = [qm_dicts[2]]

    _check_results(
        result,
        expected
    )


def test_repository_list_with_filters_unknown_operator(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    for data in qm_dicts:
        repo.createQualityMetric(QualityMetricsData.from_dict(data))

    with pytest.raises(ValueError):
        repo.listQualityMetrics(filters={'numPoints__in': [20, 30]})


def test_repository_list_with_filters_Skewed(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    for data in qm_dicts:
        repo.createQualityMetric(QualityMetricsData.from_dict(data))

    result = repo.listQualityMetrics(filters={'Skewed': 0.45})

    _check_results(
        result,
        [qm_dicts[0]]
    )


def test_repository_list_with_filters_Skewed_eq(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    for data in qm_dicts:
        repo.createQualityMetric(QualityMetricsData.from_dict(data))
    _check_results(
        repo.listQualityMetrics(filters={'Skewed__eq': 0.45}),
        [qm_dicts[0]]
    )


def test_repository_list_with_filters_Skewed_lt(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    for data in qm_dicts:
        repo.createQualityMetric(QualityMetricsData.from_dict(data))

    result = repo.listQualityMetrics(filters={'Skewed__lt': 0.499})
    _check_results(
        result,
        [qm_dicts[0], qm_dicts[1], qm_dicts[4], qm_dicts[11], qm_dicts[12]]
    )


def test_repository_list_with_filters_Skewed_gt(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    for data in qm_dicts:
        repo.createQualityMetric(QualityMetricsData.from_dict(data))

    deleteList = [qm_dicts[0], qm_dicts[1], qm_dicts[4], qm_dicts[11], qm_dicts[12]]
    newList = [qm for qm in qm_dicts if qm not in deleteList]

    _check_results(
        repo.listQualityMetrics(filters={'Skewed__gt': 0.499}),
        newList
    )


def test_repository_list_with_filters_id(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    for data in qm_dicts:
        repo.createQualityMetric(QualityMetricsData.from_dict(data))

    _check_results(
        repo.listQualityMetrics(filters={'id': '27f76aa3-29d9-b39b-f9ff-d208e01c71b5'}),
        [qm_dicts[6]]
    )


def test_repository_list_with_filters_multple_entries(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    for data in qm_dicts:
        repo.createQualityMetric(QualityMetricsData.from_dict(data))

    result = repo.listQualityMetrics(filters={'Skewed__lt': 0.499, 'Striated__lt': 0.033, })
    _check_results(
        result,
        [qm_dicts[11]]
    )

    # _check_results(
    #     repo.listQualityMetrics(filters={'numClasses__gt': 2, 'numClasses__lt': 100, }),
    #     [qm_dicts[4]]
    # )


def test_repository_delete(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    repo.createQualityMetric(QualityMetricsData.from_dict(qm_dicts[0]))
    repo.deleteQualityMetric(qm_dicts[0]['id'])

    _check_results(
        repo.listQualityMetrics(),
        []
    )


def test_repository_update(setupConnection, qm_dicts):
    repo = arangorepo.ArangoRepo(setupConnection)
    repo.connectToDatabase('scatterplots')
    repo.setCollection('testqualitymetrics')
    repo.clearCollection('testqualitymetrics')

    qm = QualityMetricsData.from_dict(qm_dicts[0])
    repo.createQualityMetric(qm)

    qm.domainMin = -800
    qm.domainMax = 1250
    repo.updateQualityMetric(qm)

    _check_results(
        repo.listQualityMetrics(filters={'id': qm.id}),
        [qm.to_dict()]
    )
