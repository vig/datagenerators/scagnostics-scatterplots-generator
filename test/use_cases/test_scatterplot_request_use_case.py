from scagnosticscnn.use_cases import scatterplot_use_cases as uc
from scagnosticscnn.domain.generator import datagenerator as dg
from scagnosticscnn.use_cases import request_objects as req
from scagnosticscnn.shared import response_object as resp


def test_scatterplot_request_without_parameters():
    generator = dg.RandomDataGenerator()

    assert generator

    scatterplot_request_use_case = uc.ScatterplotGenerateRequestUseCase(generator)

    request_object = req.ScatterplotGenerateRequestObject.from_dict({})
    response_object = scatterplot_request_use_case.execute(request_object)

    assert response_object

    assert bool(response_object) is True
    assert response_object.type == resp.ResponseSuccess.SUCCESS
    assert response_object.value


def test_scatterplot_request_with_parameters():
    generator = dg.RandomDataGenerator()

    assert generator

    scatterplot_request_use_case = uc.ScatterplotGenerateRequestUseCase(generator)

    request_object = req.ScatterplotGenerateRequestObject.from_dict(
        {'filters': {
            'count': 10000,
            'numPoints': 100,
            'domainMin': 0,
            'domainMax': 10000,
            'numClasses': 5
        }}
    )
    response_object = scatterplot_request_use_case.execute(request_object)
    assert bool(response_object) is True
    assert len(response_object.value) == 10000
