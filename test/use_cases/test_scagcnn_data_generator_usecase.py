from scagnosticscnn.use_cases import generate_scagcnn_data_use_case as uc
from scagnosticscnn.use_cases import request_objects as req

from scagnosticscnn.domain.qualitymetrics.qualitymetrics import QualityMetricsFactory
from scagnosticscnn.domain.qualitymetrics.scagnosticservice import ScagnosticsServiceBuilder

from scagnosticscnn.domain.generator import datagenerator as dg

from scagnosticscnn.repository.memrepo_scatterplot import MemRepo
from scagnosticscnn.repository.memrepo_qm import MemRepoQualityMetrics

import pytest


@pytest.fixture
def setup():
    input = {
        "generator": dg.RandomDataGenerator(),
        "factory": QualityMetricsFactory(),
        "samplingFunction": 'naive',  # lhs
        "qmStore": MemRepoQualityMetrics([]),
        "scatterplotStore": MemRepo([])
    }

    input["factory"].register_builder(
        'SCAGNOSTICS', ScagnosticsServiceBuilder())

    return input


def test_usecase(setup):
    use_case = uc.GenerateScagnosticsCNNDataUseCase(
        setup["generator"], setup["factory"], setup["samplingFunction"], setup["qmStore"], setup["scatterplotStore"])
    request_object = req.ScagnosticsCNNDataRequestObject.from_dict(
        {
            'qualitymetric': 'SCAGNOSTICS',
            'numSamples': 2,
            'delta': 0.05,
            'maxIterations': 100,
            'filters': {
                'numPoints': 1000,
                'domainMin': 0,
                'domainMax': 1000,
                'numClasses': 1,
            }
        }
    )

    try:
        response_object = use_case.execute(request_object)
    except Exception as e:
        print(e)
        pytest.fail("error during use case : {}".format(e))

    assert bool(response_object) is True


def test_sampling_method_naive(setup):
    use_case = uc.GenerateScagnosticsCNNDataUseCase(
        # datagenerator, qmfactory, samplingFunction, qmStore, scatterplotStore):
        setup["generator"], setup["factory"], setup["samplingFunction"], setup["qmStore"], setup["scatterplotStore"])
    assert use_case

    factory = setup["factory"]
    service = factory.create("SCAGNOSTICS")

    sample = use_case.generate_sample(service.getQMDefintionInterface())
    assert len(sample) == 9


def test_sampling_method_lhs(setup):
    use_case = uc.GenerateScagnosticsCNNDataUseCase(
        # datagenerator, qmfactory, samplingFunction, qmStore, scatterplotStore):
        setup["generator"], setup["factory"], 'lhs', setup["qmStore"], setup["scatterplotStore"])
    assert use_case

    factory = setup["factory"]
    service = factory.create("SCAGNOSTICS")

    for i in range(400):
        sample = use_case.generate_sample(service.getQMDefintionInterface())
        assert len(sample) == 9


def test_generate_qmfilter_from_sampling(setup):
    use_case = uc.GenerateScagnosticsCNNDataUseCase(
        # datagenerator, qmfactory, samplingFunction, qmStore, scatterplotStore):
        setup["generator"], setup["factory"], setup["samplingFunction"], setup["qmStore"], setup["scatterplotStore"])

    factory = setup["factory"]
    service = factory.create("SCAGNOSTICS")

    sample = use_case.generate_sample(service.getQMDefintionInterface())

    delta = 0.05
    qmFilter = use_case.generateQMFilter(service.getQMDefintionInterface(), sample, delta)

    assert len(qmFilter) == 9

    for filter in qmFilter:
        values_view = filter.values()
        value_iterator = iter(values_view)
        value = next(value_iterator)

        keys_view = filter.keys()
        keys_iterator = iter(keys_view)
        key = next(keys_iterator)

        ranges = service.getQMDefintionInterface().get(key, None)
        assert value[0] >= ranges[0]
        assert value[1] <= ranges[1]


def test_setup_fixture(setup):
    assert setup["generator"]
    assert setup["factory"]
    factory = setup["factory"]
    service = factory.create("SCAGNOSTICS")
    assert service
    assert setup["samplingFunction"]
    assert setup["qmStore"]
    assert setup["scatterplotStore"]
