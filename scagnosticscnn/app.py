from flask import Flask

from scagnosticscnn.rest import documentation
from scagnosticscnn.rest import scatterplot

from scagnosticscnn.rest import storageroom
from scagnosticscnn.settings import ProdConfig


def create_app(config_object=ProdConfig):
    app = Flask(__name__)
    app.config.from_object(config_object)
    app.register_blueprint(scatterplot.blueprint)
    app.register_blueprint(documentation.blueprint)
    app.register_blueprint(storageroom.blueprint)
    return app
