import json
from flask import Blueprint, request, Response

from scagnosticscnn.use_cases import request_objects as req
from scagnosticscnn.shared import response_object as res
from scagnosticscnn.repository import memrepo as mr
from scagnosticscnn.domain.generator import datagenerator as dg
from scagnosticscnn.use_cases import scatterplot_use_cases as uc
from scagnosticscnn.serializers import scatterplot_serializer as ser
import scagnosticscnn

blueprint = Blueprint('scatterplot', __name__)

STATUS_CODES = {
    res.ResponseSuccess.SUCCESS: 200,
    res.ResponseFailure.RESOURCE_ERROR: 404,
    res.ResponseFailure.PARAMETERS_ERROR: 400,
    res.ResponseFailure.SYSTEM_ERROR: 500
}


@blueprint.route('/scatterplot', methods=['GET'])
def scatterplot():
    qrystr_params = {
        'filters': {},
    }

    for arg, values in request.args.items():
        if arg.startswith('filter_'):
            qrystr_params['filters'][arg.replace('filter_', '')] = values

    request_object = req.ScatterplotGenerateRequestObject.from_dict(qrystr_params)

    generator = dg.RandomDataGenerator()
    use_case = uc.ScatterplotGenerateRequestUseCase(generator)

    response = use_case.execute(request_object)

    return Response(json.dumps(response.value, cls=ser.ScatterplotEncoder),
                    mimetype='application/json',
                    status=STATUS_CODES[response.type])
