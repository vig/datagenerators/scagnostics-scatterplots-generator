import json
from flask import Blueprint, request, Response

from scagnosticscnn.shared import response_object as res

import scagnosticscnn


STATUS_CODES = {
    res.ResponseSuccess.SUCCESS: 200,
    res.ResponseFailure.RESOURCE_ERROR: 404,
    res.ResponseFailure.PARAMETERS_ERROR: 400,
    res.ResponseFailure.SYSTEM_ERROR: 500
}

blueprint = Blueprint('documentation', __name__)


@blueprint.route('/', methods=['GET'])
def index():
    response = {
        "name": "ScagnosticsCNN Scagnostics Neural Net",
        "package version": scagnosticscnn.__version__
    }
    return Response(json.dumps(response),
                    mimetype='application/json',
                    status=STATUS_CODES[res.ResponseSuccess.SUCCESS])
