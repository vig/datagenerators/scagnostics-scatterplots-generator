from abc import ABC, abstractmethod


class QMService(ABC):

    @abstractmethod
    def calculateQM(self, scatterplot):
        """Generates the QM for the input scatterplot

        Args:
            scatterplot (scagnosticscnn.domain.ScatterPlot): The scatterplot object on which the QM should be calculated

        Raises:
            NotImplementedError: if not implemented; needs to be implemented by the subclasses of QMService
        """
        raise NotImplementedError("Needs to be implemented by QMServices")

    @abstractmethod
    def getQMDefintionInterface(self):
        """Outputs how the qm result will look like, e.g., how many and which features and which feature bounds are given.

        Returns: a simple dict with keys and value ranges

        Raises:
            NotImplementedError: if not implemented; needs to be implemented by the subclasses of QMService
        """
        raise NotImplementedError("Needs to be implemented by QMServices")
