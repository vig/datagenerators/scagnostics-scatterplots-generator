import collections

try:
    collectionsAbc = collections.abc
except AttributeError:
    collectionsAbc = collections

from scagnosticscnn.shared import request_object as req


class StorageRoomListRequestObject(req.ValidRequestObject):

    def __init__(self, filters=None):
        self.filters = filters

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if 'filters' in adict and not isinstance(adict['filters'], collectionsAbc.MutableMapping):
            invalid_req.add_error('filters', 'Is not iterable')

        if invalid_req.has_errors():
            return invalid_req

        return StorageRoomListRequestObject(filters=adict.get('filters', None))


class ScatterplotGenerateRequestObject(req.ValidRequestObject):

    def __init__(self, filters=None):
        self.filters = filters

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if 'filters' in adict and not isinstance(adict['filters'], collectionsAbc.MutableMapping):
            invalid_req.add_error('filters', 'Is not iterable')

        if 'filters' in adict and isinstance(adict['filters'], collectionsAbc.MutableMapping):
            filters = adict['filters']

            if not 'numPoints' in filters:
                invalid_req.add_error(
                    'filters', 'Does not contain number of requested scatterplot points [numPoints attribute]')

            if not 'count' in filters:
                invalid_req.add_error(
                    'filters', 'Does not contain number of requested Scatterplot [count attribute]')

            if not 'domainMin' in filters:
                invalid_req.add_error(
                    'filters', 'Does not contain the domain min for the scatterplot [domainMin attribute]')

            if not 'domainMax' in filters:
                invalid_req.add_error(
                    'filters', 'Does not contain the domain max for the scatterplot [domainMax attribute]')

            if not 'numClasses' in filters:
                invalid_req.add_error(
                    'filters', 'Does not contain number of requested classes mapped to the Scatterplot [numClasses attribute]')

        if invalid_req.has_errors():
            return invalid_req

        return ScatterplotGenerateRequestObject(filters=adict.get('filters', None))


class QualityMetricsRequestObject(req.ValidRequestObject):

    def __init__(self, config=None, scatterplot=None, qualitymetric=None):
        self.config = config
        self.scatterplot = scatterplot
        self.qualitymetric = qualitymetric.lower()

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if 'config' in adict and not isinstance(adict['config'], collectionsAbc.MutableMapping):
            invalid_req.add_error('config', 'Is not iterable')

        if not 'scatterplot' in adict:
            invalid_req.add_error(
                'scatterplot', 'There is no scatterplot to compute quality metrics on')

        if not 'qualitymetric' in adict:
            invalid_req.add_error(
                'qualitymetric', 'Unclear which quality metric should be used')

        if invalid_req.has_errors():
            return invalid_req

        return QualityMetricsRequestObject(config=adict.get('config', None), scatterplot=adict.get('scatterplot', None), qualitymetric=adict.get('qualitymetric', None))


class ScatterplotWithQMRequestObject(req.ValidRequestObject):

    def __init__(self, filters=None):
        self.filters = filters

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if 'filters' in adict and not isinstance(adict['filters'], collectionsAbc.MutableMapping):
            invalid_req.add_error('filters', 'Is not iterable')

        # if 'filters' in adict and isinstance(adict['filters'], collectionsAbc.MutableMapping):
        #     filters = adict['filters']

        #     if not 'numPoints' in filters:
        #         invalid_req.add_error(
        #             'filters', 'Does not contain number of requested scatterplot points [numPoints attribute]')

        #     if not 'count' in filters:
        #         invalid_req.add_error(
        #             'filters', 'Does not contain number of requested Scatterplot [count attribute]')

        #     if not 'domainMin' in filters:
        #         invalid_req.add_error(
        #             'filters', 'Does not contain the domain min for the scatterplot [domainMin attribute]')

        #     if not 'domainMax' in filters:
        #         invalid_req.add_error(
        #             'filters', 'Does not contain the domain max for the scatterplot [domainMax attribute]')

        #     if not 'numClasses' in filters:
        #         invalid_req.add_error(
        #             'filters', 'Does not contain number of requested classes mapped to the Scatterplot [numClasses attribute]')

        if invalid_req.has_errors():
            return invalid_req

        return ScatterplotWithQMRequestObject(filters=adict.get('filters', None))


class ScagnosticsCNNDataRequestObject(req.ValidRequestObject):
    def __init__(self, config=None, filters=None, qualitymetric=None, delta=0.05, maxIterations=1000, numSamples=None):
        self.filters = filters
        self.config = config
        self.qualitymetric = qualitymetric
        self.numSamples = numSamples
        self.delta = delta
        self.maxIterations = maxIterations

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if 'filters' in adict and not isinstance(adict['filters'], collectionsAbc.MutableMapping):
            invalid_req.add_error('filters', 'Is not iterable')

        if 'numSamples' not in adict:
            invalid_req.add_error('numSamples', 'required')

        if invalid_req.has_errors():
            return invalid_req

        return ScagnosticsCNNDataRequestObject(maxIterations=adict.get('maxIterations', None), numSamples=adict.get('numSamples', None), delta=adict.get('delta', None), config=adict.get('config', None), filters=adict.get('filters', None), qualitymetric=adict.get('qualitymetric', None))
