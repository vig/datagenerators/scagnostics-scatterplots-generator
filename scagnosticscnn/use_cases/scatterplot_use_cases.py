from scagnosticscnn.shared import use_case as uc, qualitymetrics_use_cases as quc, response_object as res
from scagnosticscnn.domain.scatterplot import ScatterPlot

from scagnosticscnn.use_cases.qualitymetrics_use_cases import QualityMetricsRequestUseCase
from itertools import repeat
from random import random, randint

# Parallelizing with Pool.starmap()
import multiprocessing as mp
from multiprocessing.pool import ThreadPool

from scagnosticscnn.shared.utils import SimulatedAnnealing

import psutil
num_cpus = psutil.cpu_count(logical=False)


class ScatterplotGenerateRequestUseCase(uc.UseCase):

    def __init__(self, generator):
        self.generator = generator

    def process_request(self, request_object):
        domain_scatterplot = self.generator.scatterplot(
            filters=request_object.filters)
        return res.ResponseSuccess(domain_scatterplot)


class ScatterplotWithQM_Serial_RequestUseCase(quc.QualityMetricsRequestUseCase):

    def __init__(self, generator, qm, recordIntermediateSteps=False, queueMaxSize=200):
        super().__init__(generator, qm, recordIntermediateSteps=recordIntermediateSteps, queueMaxSize=queueMaxSize)

    def process_request(self, request_object):

        filters = request_object.filters
        countScatterPlots = filters['count']
        filters['count'] = 1

        qmFilters = filters['qmfilters']

        resultList = []
        while len(resultList) < countScatterPlots:
            scatterplot_data = self.generator.scatterplot(
                filters=request_object.filters)[0]

            domain_scatterplot = ScatterPlot.from_dict(scatterplot_data)
            qmresult = self.qm.calculateQM(domain_scatterplot)
            self.putInQueue(domain_scatterplot, qmresult)
            if self._isInFilterRange(qmresult, qmFilters):
                resultList.append(domain_scatterplot)
        return res.ResponseSuccess(resultList)


class ScatterplotWithQM_Parallel_RequestUseCase(quc.QualityMetricsRequestUseCase):

    def __init__(self, generator, qm, recordIntermediateSteps=False, queueMaxSize=200):
        super().__init__(generator, qm, recordIntermediateSteps=recordIntermediateSteps, queueMaxSize=queueMaxSize)

    def process_request(self, request_object):

        filters = request_object.filters
        countScatterPlots = filters['count']
        filters['count'] = 1

        qmFilters = filters['qmfilters']

        iterable = repeat((request_object, qmFilters), countScatterPlots)
        with ThreadPool(20) as pool:
            outputs = pool.starmap(self.get_value, iterable)
        return res.ResponseSuccess(outputs)

    def get_value(self, request_object, qmFilters):
        inRange = False

        while not inRange:
            scatterplot_data = self.generator.scatterplot(filters=request_object.filters)[0]
            domain_scatterplot = ScatterPlot.from_dict(scatterplot_data)
            qmresult = self.qm.calculateQM(domain_scatterplot)

            self.putInQueue(domain_scatterplot, qmresult)
            if self._isInFilterRange(qmresult, qmFilters):
                inRange = True
                return domain_scatterplot




class ScatterplotWithQM_Permutation_RequestUseCase(quc.QualityMetricsRequestUseCase):
    """Permutation based Scatterplot adaption inspired by the paper

    Same Stats, Different Graphs: Generating Datasets with Varied Appearance and Identical Statistics through Simulated Annealing
    Justin Matejka and George Fitzmaurice Autodesk Research, Toronto Ontario Canada
    """

    def __init__(self, generator, qm, maxIterations=100000, recordIntermediateSteps=False, queueMaxSize=200, maxThreadpool=20):
        self._maxIterations = maxIterations
        self.maxThreadpool = maxThreadpool
        super().__init__(generator, qm, recordIntermediateSteps=recordIntermediateSteps, queueMaxSize=queueMaxSize)

    def process_request(self, request_object):
        filters = request_object.filters
        countScatterPlots = filters['count']
        filters['count'] = 1

        qmFilters = filters['qmfilters']

        iterable = repeat((request_object, qmFilters), countScatterPlots)
        # with mp.Pool(mp.cpu_count()) as pool:
        with ThreadPool(self.maxThreadpool) as pool:
            outputs = pool.starmap(self.get_value, iterable)
        return res.ResponseSuccess(outputs)

    def get_value(self, request_object, qmFilters):
        inRange = False

        while not inRange:
            scatterplot_data = self.generator.scatterplot(filters=request_object.filters)[0]
            initial_scatterplot = ScatterPlot.from_dict(scatterplot_data)

            current_scatterplot = initial_scatterplot
            qmresult_current = self.qm.calculateQM(current_scatterplot)
            errorCurrent = self.error(qmresult_current, qmFilters)

            simAnnealing = SimulatedAnnealing(self._maxIterations, errorCurrent)

            for i in range(self._maxIterations):
                test_scatterplot = self.perturb(current_scatterplot, qmFilters)
                qmresult_test = self.qm.calculateQM(test_scatterplot)
                deltaE = self.error(qmresult_test, qmFilters)

                if simAnnealing.shouldAccept(deltaE):
                    current_scatterplot = test_scatterplot
                    qmresult_current = qmresult_test
                    errorCurrent = deltaE
                    self.putInQueue(current_scatterplot, qmresult_current)

                    if self._isInFilterRange(qmresult_current, qmFilters):
                        return current_scatterplot

                simAnnealing.updateTemp()

            if self._isInFilterRange(qmresult_current, qmFilters):
                inRange = True
            else:
                # raise RuntimeWarning("this config could not be found after maxiterations")
                return None

        return current_scatterplot

    def perturb(self, current_scatterplot, qmFilters):
        test_scatterplot = self.moveRandomPoints(
            ScatterPlot.from_dict(current_scatterplot.to_dict()))
        return test_scatterplot

    def moveRandomPoints(self, scatterplot):
        rangeDomain = scatterplot.domainMax - scatterplot.domainMin

        maxStep = rangeDomain * 0.05
        minStep = -maxStep

        modPointsNumber = int(scatterplot.numPoints * random())
        for index in range(modPointsNumber):
            modPoint = scatterplot.points[index]
            rand_x = randint(minStep, maxStep)
            rand_y = randint(minStep, maxStep)
            if modPoint[0] + rand_x < scatterplot.domainMax and modPoint[0] + rand_x > scatterplot.domainMin:
                modPoint[0] = modPoint[0] + rand_x
            if modPoint[1] + rand_y < scatterplot.domainMax and modPoint[1] + rand_y > scatterplot.domainMin:
                modPoint[1] = modPoint[1] + rand_y

        return scatterplot

    def error(self, qmresult, qmFilters):
        errorValue = 0.0  # being maximally off for nine scagnostics values == 9.0

        for filter in qmFilters:
            for qmName, qmRange in filter.items():
                target = qmRange[0] + (qmRange[1] - qmRange[0]) / 2
                errorValue -= abs(qmresult[qmName] - target)

        return errorValue
