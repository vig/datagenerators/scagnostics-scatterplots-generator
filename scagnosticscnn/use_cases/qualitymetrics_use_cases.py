from scagnosticscnn.shared import use_case as uc
from scagnosticscnn.shared import response_object as res
from scagnosticscnn.domain.scatterplot import ScatterPlot
from scagnosticscnn.domain.qmdata import QualityMetricsData


class QualityMetricsRequestUseCase(uc.UseCase):

    def __init__(self, factory):
        self.factory = factory

    def process_request(self, request_object):
        # request_object = req.QualityMetricsRequestObject.from_dict(
        #         {'scatterplot': scatterplot,
        #          'qualitymetric': 'SCAGNOSTICS',
        #          'config': config})

        domain_scatterplot = ScatterPlot.from_dict(request_object.scatterplot)

        qmService = None
        if request_object.config:
            qmService = self.factory.create(request_object.qualitymetric, **request_object.config)
        else:
            qmService = self.factory.create(request_object.qualitymetric)

        qmResult = qmService.calculateQM(domain_scatterplot)

        return res.ResponseSuccess(qmResult)
