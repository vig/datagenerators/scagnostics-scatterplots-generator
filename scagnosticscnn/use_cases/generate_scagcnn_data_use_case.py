from scagnosticscnn.shared import use_case as uc

from scagnosticscnn.use_cases.scatterplot_use_cases import ScatterplotWithQM_Permutation_RequestUseCase
from scagnosticscnn.shared import response_object as res
from scagnosticscnn.domain.scatterplot import ScatterPlot

from scagnosticscnn.use_cases import request_objects as req

import numpy as np
import random

import lhsmdu
# from itertools import repeat

# # Parallelizing with Pool.starmap()
# import multiprocessing as mp
# from multiprocessing.pool import ThreadPool

import logging


class GenerateScagnosticsCNNDataUseCase(uc.UseCase):

    def __init__(self, datagenerator, qmfactory, samplingMethod, qmStore, scatterplotStore, delta=0.05):
        self.qmfactory = qmfactory
        self.datagenerator = datagenerator
        self.samplingMethod = samplingMethod
        self.qmStore = qmStore
        self.spStore = scatterplotStore
        self.lhsmduInit = False
        self.delta = delta

    def process_request(self, request_object):
        if not request_object.filters:
            raise ValueError(
                "There needs to be a filters object defining how the scatter plots look like")
        if not request_object.numSamples:
            raise ValueError("Unclear how many samples should be generated; use numSamples")
        if not request_object.delta:
            raise ValueError("Unclear how strict the processing needs to be; use delta")
        if not request_object.maxIterations:
            raise ValueError("Unclear how often a permutation should be attempted; use maxIterations")

        qmService = None
        if request_object.config:
            qmService = self.qmfactory.create(request_object.qualitymetric, **request_object.config)
        else:
            qmService = self.qmfactory.create(request_object.qualitymetric)

        qmDefinition = qmService.getQMDefintionInterface()

        scatterplot_with_qm_request_use_case = ScatterplotWithQM_Permutation_RequestUseCase(
            self.datagenerator, qmService, maxIterations=request_object.maxIterations)

        done = 0
        filters = request_object.filters
        while done < request_object.numSamples:
            sample = self.generate_sample(qmDefinition)

            qmFilters = self.generateQMFilter(
                qmDefinition, sample, request_object.delta)

            request_object_internal = req.ScatterplotWithQMRequestObject.from_dict(
                {'filters': {
                    'count': 1,
                    'numPoints': filters['numPoints'],
                    'domainMin': filters['domainMin'],
                    'domainMax': filters['domainMax'],
                    'numClasses': filters['numClasses'],
                    'qmfilters': qmFilters
                }}
            )

            response_object = scatterplot_with_qm_request_use_case.execute(request_object_internal)
            for sp in response_object.value:
                if sp:
                    self.qmStore.save(sp)
                    self.spStore.save(sp)
                else:
                    logging.getLogger().info(f"This sample is not achievable: {sample}")
                    print(f"This sample is not achievable: {sample}")

            done = done + 1

        return res.ResponseSuccess([])

    def generate_sample(self, qmDefinition):
        sample = []

        if self.samplingMethod == 'naive':
            for key in qmDefinition.keys():
                ranges = qmDefinition.get(str(key), None)
                sample.append(random.uniform(ranges[0], ranges[1]))
        elif self.samplingMethod == 'lhs':
            featureLength = len(list(qmDefinition.keys()))
            if not self.lhsmduInit:
                self.lhsmduInit = True
                sample = lhsmdu.sample(featureLength, 1)
            else:
                sample = lhsmdu.resample()
        else:
            raise ValueError('This sampling method name is not implemented use naive or lhs')
        return sample

    def generateQMFilter(self, qmDefinition, sample, delta):
        if len(qmDefinition.keys()) != len(sample):
            raise ValueError("qmDefinition.keys() and sample array must be of the same length")
        qmFilter = []

        keys = list(qmDefinition.keys())
        for i in range(len(keys)):
            # [{'Clumpy': [0.0, 0.2]}]
            keyName = keys[i]
            midPoint = sample[i]

            lowPoint = max(qmDefinition[keyName][0], midPoint - delta)
            highPoint = min(qmDefinition[keyName][1], midPoint + delta)

            qmFilter.append({
                keyName: [lowPoint, highPoint]
            })
        return qmFilter
