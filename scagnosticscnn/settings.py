import os


class Config(object):
    """Base configuration."""

    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))

    ARANBGO_DB_PORT = os.getenv('ARANBGO_DB_PORT', 8529)
    ARANBGO_DB_URL = os.getenv('ARANBGO_DB_URL', "131.211.81.100")
    ARANBGO_DB_USER = os.getenv('ARANBGO_DB_USER', "scatterplotuser")
    ARANBGO_DB_PW = os.getenv('ARANBGO_DB_PW', "scatterplotuser")


class ProdConfig(Config):
    """Production configuration."""
    ENV = 'prod'
    DEBUG = False
    ARANBGO_DB_URL = os.getenv('ARANBGO_DB_URL', "131.211.81.100")


class DevConfig(Config):
    """Development configuration."""
    ENV = 'dev'
    DEBUG = True
    ARANBGO_DB_URL = os.getenv('ARANBGO_DB_URL', "localhost")


class TestConfig(Config):
    """Test configuration."""
    ENV = 'test'
    TESTING = True
    DEBUG = True
    ARANBGO_DB_URL = os.getenv('ARANBGO_DB_URL', "localhost")
    ARANBGO_DB_USER = os.getenv('ARANBGO_DB_USER', "scatterplotuser")
    ARANBGO_DB_PW = os.getenv('ARANBGO_DB_PW', "scatterplotuser")
