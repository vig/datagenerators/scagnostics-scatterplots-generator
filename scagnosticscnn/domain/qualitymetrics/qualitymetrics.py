class QualityMetricsFactory:
    def __init__(self):
        self._builders = {}

    def register_builder(self, key, builder):
        self._builders[key.lower()] = builder

    def create(self, key, **kwargs):
        builder = self._builders.get(key.lower())
        if not builder:
            raise ValueError(key)
        return builder(**kwargs)
