from pyscagnostics import scagnostics
from scagnosticscnn.domain.scatterplot import ScatterPlot
from scagnosticscnn.domain.qmdata import QualityMetricsData

from scagnosticscnn.shared.abstract_qm_service import QMService

import numpy as np
import pandas as pd
import inspect
import functools


class ScagnosticsService(QMService):
    """
    # Using NumPy arrays or lists
    x = np.random.uniform(0, 1, 100)
    y = np.random.uniform(0, 1, 100)

    measures, _ = scagnostics(x, y)
    print(measures)

    # Using Pandas DataFrame
    all_measures = scagnostics(df)
    # for measures, _ in all_measures:
        print(measures)
    """

    def __init__(self, **kwargs):
        """
        Constructor for ScagnosticsService 

        Args:
            kwargs with scagnostics_bins for bins
            kwargs with scagnostics_remove_outliers for setting outlier removal True/False
        """
        self.bins = kwargs.get("scagnostics_bins", 50)
        if self.bins <= 1:
            raise ValueError("The Scagnostics bins should be more than 1")
        self.remove_outliers = kwargs.get("scagnostics_remove_outliers", True)

    def _calculateQMDataFrame(self, df):
        all_measures = scagnostics(df, bins=self.bins, remove_outliers=self.remove_outliers)
        print(f'Scagnotiscs with parameter {all_measures}')
        return all_measures

    # @functools.lru_cache(maxsize=128)
    def calculateQM(self, scatterplot):
        if not scatterplot:
            raise ValueError("Cannot be invoked without scatterplot")

        df = ScagnosticsService.scatterplot_to_dataframe(scatterplot)
        qmdict = self.getOnlyScagDict(self._calculateQMDataFrame(df))
        qmdict['id'] = scatterplot.id
        qmdict['type'] = 'Scagnostics'

        return QualityMetricsData.from_dict(qmdict)

    def getOnlyScagDict(self, results):
        if not results.__name__ == "genexpr":
            raise ValueError('Can only be invoked with a PyScagnostics result generator type')

        x, y, result = next(results)
        measures, bins = result
        return measures

    @classmethod
    def scatterplot_to_dataframe(cls, scatterplot):
        if not isinstance(scatterplot, ScatterPlot):
            raise ValueError("can only be invoked with a ScatterPlot instance")
        points = scatterplot.points
        df = pd.DataFrame(points)
        return df

    def getQMDefintionInterface(self):
        dict = {
            'Outlying': [0, 1],
            'Skewed': [0, 1],
            'Clumpy': [0, 1],
            'Sparse': [0, 1],
            'Striated': [0, 1],
            'Convex': [0, 1],
            'Skinny': [0, 1],
            'Stringy': [0, 1],
            'Monotonic': [0, 1]
        }

        return dict


class ScagnosticsServiceBuilder:
    def __init__(self):
        self._instance = None

    def __call__(self, **kwargs):
        # Singleton implementation
        # if not self._instance:
        #     self._instance = ScagnosticsService(**kwargs)
        self._instance = ScagnosticsService(**kwargs)
        return self._instance
