
from mimesis.schema import Field, Schema
from mimesis.random import Random as MimesisRandom

# from flask import jsonify, request, Response

import numpy


class RandomDataGenerator(object):

    def __init__(self, seed=None):
        if seed and isinstance(seed, int):
            self._seed = seed
        else:
            self._seed = 10003
        numpy.random.seed(self._seed)
        self._random = numpy.random

    def scatterplot(self, filters=None):
        if filters is None:
            filters = dict()
        count = filters.get('count', 1)
        domainMin = filters.get('domainMin', 0)
        domainMax = filters.get('domainMax', 1000)
        numPoints = filters.get('numPoints', 100)
        numClasses = filters.get('numClasses', 1)
        locale = filters.get('locale', 'en')

        _ = Field(locale)

        schema = Schema(schema=lambda: {
            'id': _('uuid'),
            'datasetName': "numpy.random.random_integers",
            'domainMin': domainMin,
            'domainMax': domainMax,
            'numPoints': numPoints,
            'numClasses': numClasses,
            # 'version': _('version', pre_release=True),
            'timestamp': _('timestamp', posix=False),
            'points': self._createDataPoints(domainMin, domainMax, numPoints),
            'classes': self._createClasses(numPoints, numClasses),
        })
        data = schema.create(iterations=count)
        return data

    def _createDataPoints(self, domainMin=0, domainMax=1000, numPoints=100):
        # rand = Random()
        # miRand = MimesisRandom()
        # result = miRand.randints(numPoints, 0, domain)
        random_integer_array = self._random.randint(
            domainMin, domainMax, size=(numPoints, 2))
        return random_integer_array.tolist()

    def _createClasses(self, numPoints=100, numClasses=1):
        # rand = Random()
        # miRand = MimesisRandom()
        # result = miRand.randints(numPoints, 0, domain)
        random_classes_array = self._random.randint(
            0, numClasses, size=numPoints).tolist()
        return random_classes_array
