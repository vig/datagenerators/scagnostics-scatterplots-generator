import json


class ScatterplotEncoder(json.JSONEncoder):

    def default(self, o):
        try:
            to_serialize = {
                'id': str(o.id),
                'domainMin': o.domainMin,
                'domainMax': o.domainMax,
                "points": o.points,
                "numPoints": o.numPoints,
                "classes": o.classes,
                "numClasses": o.numClasses,
                "timestamp": o.timestamp,
                "datasetName": o.datasetName,
            }
            return to_serialize
        except AttributeError:
            return super().default(o)
