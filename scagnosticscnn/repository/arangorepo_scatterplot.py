from scagnosticscnn.domain import scatterplot as sp
from scagnosticscnn.domain.qmdata import QualityMetricsData

from arango_crud import (
    Config, BasicAuth, RandomCluster, StepBackOffStrategy
)

from pyArango.connection import *


from scagnosticscnn.settings import Config as basicconfig

import json


class ArangoRepo:

    operatorMap = {
        "eq": "==",
        "lt": "<",
        "gt": ">",
    }

    def __init__(self, config=None):

        if not config:
            self.config = Config(
                cluster=RandomCluster(urls=[basicconfig.ARANBGO_DB_URL + ':' +
                                            str(basicconfig.ARANBGO_DB_PORT)]),  # see Cluster Styles
                timeout_seconds=3,
                back_off=StepBackOffStrategy([0.1, 0.5, 1, 1, 1]),  # see Back Off Strategies
                auth=BasicAuth(username=basicconfig.ARANBGO_DB_USER,
                               password=basicconfig.ARANBGO_DB_PW),
                ttl_seconds=31622400
            )
            self.username = basicconfig.ARANBGO_DB_USER
            self.password = basicconfig.ARANBGO_DB_PW

        else:
            self.config = config
            self.username = config.auth.username
            self.password = config.auth.password

    def _check(self, element, key, value):
        if '__' not in key:
            key = key + '__eq'

        key, operator = key.split('__')

        if operator not in ['eq', 'lt', 'gt']:
            raise ValueError('Operator {} is not supported'.format(operator))

        operator = '__{}__'.format(operator)

        if key in ['domainMin', 'domainMax', 'numPoints', 'numClasses']:
            return getattr(element[key], operator)(int(value))
        elif key in ['id', 'datasetName', 'timestamp']:
            return getattr(element[key], operator)(str(value))

        return getattr(element[key], operator)(value)

    def listScatterplots(self, filters=None):
        if not filters:
            if not self.collectionName or self.collectionName == '':
                raise ValueError("Unclear which collection should be listed; use setCollection")
            collection = self.db[self.collectionName]
            result = collection.fetchAll()
        else:
            dummySP = sp.ScatterPlot("dummy", 0, 1, [], [])
            filterstring = "FILTER "
            for key, value in filters.items():
                if '__' not in key:
                    key = key + '__eq'

                key, operator = key.split('__')
                if not hasattr(dummySP, key):
                    raise KeyError(
                        "Scatterplots cannot be filtered/queried by this key; SP does not contain this key: " + key)
                else:
                    if operator not in ['eq', 'lt', 'gt']:
                        raise ValueError('Operator {} is not supported'.format(operator))

                    predicate = ""
                    if not filterstring.endswith("FILTER "):
                        predicate += " AND "

                    if key in ['id', 'datasetName', 'timestamp']:
                        predicate += "doc." + key + " " + \
                            self.operatorMap[operator] + " '" + str(value) + "'"
                    else:
                        predicate += "doc." + key + " " + \
                            self.operatorMap[operator] + " " + str(value)

                    filterstring += predicate
                aql = "FOR doc IN {} {} RETURN doc".format(self.collectionName, filterstring)
                result = self.db.AQLQuery(aql, rawResults=True, batchSize=100)
        return [sp.ScatterPlot.from_dict(r) for r in result]

    def listQualityMetrics(self, filters=None):
        if not filters:
            if not self.collectionName or self.collectionName == '':
                raise ValueError("Unclear which collection should be listed; use setCollection")
            collection = self.db[self.collectionName]
            result = collection.fetchAll(rawResults=True)
        else:
            filterstring = "FILTER "
            for key, value in filters.items():
                if '__' not in key:
                    key = key + '__eq'

                key, operator = key.split('__')
                if operator not in ['eq', 'lt', 'gt']:
                    raise ValueError('Operator {} is not supported'.format(operator))

                predicate = ""
                if not filterstring.endswith("FILTER "):
                    predicate += " AND "

                if key in ['id', 'type', 'timestamp'] or type(value) == type('str'):
                    predicate += "doc." + key + " " + \
                        self.operatorMap[operator] + " '" + str(value) + "'"
                else:
                    predicate += "doc." + key + " " + \
                        self.operatorMap[operator] + " " + str(value)

                filterstring += predicate
            aql = "FOR doc IN {} {} RETURN doc".format(self.collectionName, filterstring)
            result = self.db.AQLQuery(aql, rawResults=True, batchSize=100)
        return [QualityMetricsData.from_dict(r) for r in result]

    def connectToDatabase(self, dbName):
        if not dbName:
            raise ValueError("Cannot connect; DBname unknown")

        try:
            conn = Connection(
                arangoURL=self.config.cluster.urls[0], username=self.username, password=self.password)
            # conn = Connection(
            #     arangoURL='http://127.0.0.1:8529', username='scatterplotuser', password='scatterplotuser')
            if not conn.hasDatabase(dbName):
                self.db = conn.createDatabase(name=dbName)
            else:
                self.db = conn[dbName]
            self.dbName = dbName
        except Exception as e:
            print('SETUP with User={} and PW={} on URL={} failed'.format(
                self.username, self.password, self.config.cluster.urls[0]))
            print(e)

    def setCollection(self, collectionName):
        if not collectionName:
            raise ValueError("Collection name must be given")
        dbCRUD = self.config.database(self.dbName)
        self.collectionCRUD = dbCRUD.collection(collectionName)
        self.collectionCRUD.create_if_not_exists()
        self.collectionName = collectionName

    def clearCollection(self, collectionName):
        if not collectionName:
            raise ValueError("Collection name must be given")
        collection = self.db[self.collectionName]
        collection.empty()

    def checkDB(self):
        if not self.db:
            raise ValueError(
                "Not connected to any database; use ArangoRepo.connectToDatabase(dbname) beforehand")

    ############## SP #####################

    def createScatterplot(self, scatterplot):
        if not scatterplot:
            raise ValueError("No scatterplot to store")
        self.checkDB()

        spDict = scatterplot.to_dict()
        # The simplest interface
        collection = self.db[self.collectionName]
        doc1 = collection.createDocument(initDict=spDict)
        doc1._key = spDict['id']
        doc1.save()

        # self.collectionCRUD.create_or_overwrite_doc(scatterplot['id'], doc1)

    def deleteScatterplot(self, scatterplotid):
        if not scatterplotid:
            raise ValueError("No scatterplot id to delete")
        self.checkDB()

        aql = "REMOVE {{ _key:'{}' }} IN {}".format(scatterplotid, self.collectionName)
        self.db.AQLQuery(aql, rawResults=True, batchSize=100)

    def updateScatterplot(self, scatterplot):
        if not scatterplot:
            raise ValueError("No scatterplot to update")
        self.checkDB()

        spDict = scatterplot.to_dict()
        aql = "UPDATE {{ _key:'{}' }} WITH {} IN {}".format(
            spDict['id'], json.dumps(spDict), self.collectionName)
        self.db.AQLQuery(aql)

    ############## QM #####################

    def createQualityMetric(self, qualitymetric):
        if not qualitymetric:
            raise ValueError("No qualitymetric to store")
        self.checkDB()
        self.checkDB()

        qmDict = qualitymetric.to_dict()
        # The simplest interface
        collection = self.db[self.collectionName]
        doc1 = collection.createDocument(initDict=qmDict)
        doc1._key = qmDict['id']
        doc1.save()
        # self.collectionCRUD.create_or_overwrite_doc(scatterplot['id'], doc1)

    def deleteQualityMetric(self, qmid):
        if not qmid:
            raise ValueError("No qualitymetric id to delete")
        self.checkDB()

        aql = "REMOVE {{ _key:'{}' }} IN {}".format(qmid, self.collectionName)
        self.db.AQLQuery(aql, rawResults=True, batchSize=100)

    def updateQualityMetric(self, qmData):
        if not qmData:
            raise ValueError("No qualitymetric to update")
        self.checkDB()

        spDict = qmData.to_dict()
        aql = "UPDATE {{ _key:'{}' }} WITH {} IN {}".format(
            spDict['id'], json.dumps(spDict), self.collectionName)
        self.db.AQLQuery(aql)
